﻿using System;
using System.Numerics;

namespace AlgHomeWork03
{
    internal interface ITask
    {
        double Run(string[] data);
        //for fibo
        //BigInteger Run(string[] data);
    }
}
