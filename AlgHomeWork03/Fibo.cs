﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace AlgHomeWork03
{
    internal class Fibo: ITask
    {
        public long FibByO2degN(long number)
        {
            if (number <= 1)
                return number;
            return FibByO2degN(number - 1) + FibByO2degN(number - 2);
        }

        public BigInteger FibByON(long number)
        {
            BigInteger F0 = BigInteger.Zero;
            if(number == 0)
            {
                return F0;
            }
            BigInteger F1 = BigInteger.One;

            for (int i = 1; i < number; i++)
            {
                BigInteger F2 = F0 + F1;
                F0 = F1;
                F1 = F2;
            }

            return F1;
        }

        public long FibByGolderRatio(int number)
        {
            double goldenRatio = (1 + Math.Sqrt(5)) / 2;
            return (long)Math.Round(Math.Pow(goldenRatio, number) / Math.Sqrt(5));
        }

        public BigInteger Run(string[] data)
        {
            var n = Convert.ToInt32(data[0]);
            return FibByON(n);
        }

        double ITask.Run(string[] data)
        {
            throw new NotImplementedException();
        }
    }
}
