﻿using System;
using System.Globalization;
using System.Numerics;

namespace AlgHomeWork03
{
    internal class DataTester
    {
        private readonly string _path;
        private readonly ITask _task;

        public DataTester(ITask task, string path)
        {
            _path = path;
            _task = task;
        }

        public void RunTests()
        {
            var i = 0;
            while (true)
            {
                string inFilePath = $"{_path}test.{i}.in"; 
                string outFilePath = $"{_path}test.{i}.out";
                if (!File.Exists(inFilePath) ||
                    !File.Exists(outFilePath))
                {
                    break;
                }
                Console.WriteLine($"Test №{i}: " + 
                    RunTest(inFilePath, outFilePath));
                i++;
            }
        }

        private (string, bool) RunTest(string inFilePath, string outFilePath)
        {
            string[] data = File.ReadAllLines(inFilePath);
            string expect = File.ReadAllText(outFilePath).Trim();
            double actual = _task.Run(data); 
            var interm = data.Length > 1 ? $"degree = {data[1]}," : "";
            return ($"N = {data[0]}, {interm} expect = {expect}, actual = {actual}", Convert.ToDouble(expect, CultureInfo.InvariantCulture) == actual);
            // for fibo
            //BigInteger actual = _task.Run(data); // BigInteger for fibo
            //var interm = data.Length > 1 ? $"degree = {data[1]}," : "";
            //return ($"N = {data[0]}, {interm} expect = {expect}, actual = {actual}", BigInteger.Parse(expect) == actual);
        }
    }
}
