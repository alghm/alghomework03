﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace AlgHomeWork03
{
    internal class Primes : ITask
    {
        public bool[] GetPrimesByONLogLogN(int n)
        {
            bool[] primes = new bool[n + 1];
            for (int i = 0; i < primes.Length; i++)
            {
                primes[i] = true;
            }

            for (int p = 2; p * p <= n; p++)
            {
                if (primes[p] == true)
                {
                    for (int i = p * p; i <= n; i += p)
                    {
                        primes[i] = false;
                    }
                }
            }

            return primes;
        }

        public double Run(string[] data)
        {
            var n = Convert.ToInt32(data[0]);
            var primes = GetPrimesByONLogLogN(n).Skip(2).Where(n => n == true);
            return primes.Count();
        }

        /* BigInteger ITask.Run(string[] data)
        {
            throw new NotImplementedException();
        }*/
    }
}
