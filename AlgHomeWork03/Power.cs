﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace AlgHomeWork03
{
    internal class Power : ITask
    {
        public double PowerByON(double number, int degree)
        {
            if (degree == 0)
                return 1;
            if (degree == 1)
                return number;

            double result = number;
            for (int i = 1; i < degree; i++)
            {
                result *= number;
            }
            return result;
        }

        public double PowerByONd2LogN(double number, int degree)
        {
            if (degree == 0)
                return 1;
            if (degree == 1)
                return number;
            if (degree % 2 == 0)
            {
                var result = PowerByONd2LogN(number, degree / 2);
                return result * result;
            }
            else
            {
                return number * PowerByONd2LogN(number, degree - 1);
            }

        }

        public double PowerByO2LogN(double number, long degree)
        {
            if (degree == 0)
                return 1;

            double result = 1;
            while (degree > 0)
            {
                if (degree % 2 == 1)
                    result *= number;

                number *= number;
                degree /= 2;
            }

            return Math.Round(result, 11);
        }

        public double Run(string[] data)
        {
            var n = Convert.ToDouble(data[0], CultureInfo.InvariantCulture);
            var d = Convert.ToInt64(data[1]);
            return PowerByO2LogN(n, d);
        }
        
        /* BigInteger ITask.Run(string[] data)
        {
            throw new NotImplementedException();
        }*/
    }
}
